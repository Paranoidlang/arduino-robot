#include<Servo.h>
#include<MsTimer2.h>
Servo myservo;
char inByte=0;
String temp="";
  //控制舵机
int output11=11;
  //控制左电机
int output4=4;
int output5=5;
//控制右电机
int output7=7;
int output8=8;
//控制13号LED灯
int output13=13;
//初始化LED状态
bool state=false;
int delay_parama=10000;
int Pi_read;
int Move=2;
double Angle;
double rate=0.3;
int front_move=0;
double Target_A,Target_B;
//用占空比来体现速度
double differential_mechanism_L(double angle,double velocity){
     Target_A=velocity*(1-1.55*tan(angle/180*PI)/2/1.45);//左电机
     return Target_A;
  }
 double differential_mechanism_R(double angle,double velocity){
    Target_B=velocity*(1+1.55*tan(angle/180*PI)/2/1.45);//右电机
     return Target_B;
  }
void L_advance(double DC){//左轮前进
  digitalWrite(output5,LOW); 
   digitalWrite(output4,HIGH);
   delayMicroseconds((int)(delay_parama*DC));
   digitalWrite(output4,LOW);
   delayMicroseconds((int)((1-DC)*delay_parama));
  }
void R_advance(double DC){
 digitalWrite(output8,LOW);
   digitalWrite(output7,HIGH);
   delayMicroseconds((int)(delay_parama*DC));
   digitalWrite(output7,LOW);
   delayMicroseconds((int)((1-DC)*delay_parama));
  }

 void L_retreat(double DC){//后退
  digitalWrite(output4,LOW); 
   digitalWrite(output5,HIGH);
   delayMicroseconds((int)(delay_parama*DC));
   digitalWrite(output5,LOW);
   delayMicroseconds((int)((1-DC)*delay_parama));
  }
void R_retreat(double DC){//后退
 digitalWrite(output7,LOW); 
   digitalWrite(output8,HIGH);
   delayMicroseconds((int)(delay_parama*DC));
   digitalWrite(output8,LOW);
   delayMicroseconds((int)((1-DC)*delay_parama));
  }
  void L_stop(){//停止
  digitalWrite(output4,HIGH);
  digitalWrite(output5,HIGH);
  }
 void R_stop(){//停止
 digitalWrite(output7,HIGH);
  digitalWrite(output8,HIGH);
  }
  void onTimer(){
 Serial.println();
     //电机正转
    if(Move==0){  
    L_advance(differential_mechanism_L(Angle,rate));
    R_advance(differential_mechanism_R(Angle,rate));
    }
   //电机反转
   if(Move==1){
     L_retreat(differential_mechanism_L(Angle,rate));
     R_retreat(differential_mechanism_R(Angle,rate));
    }
   if(Move==2){//直接停止小车的前进
      L_stop();
      R_stop();
      }
          }
   void control_rate(int move){//对于初始电机占空比 
      if(move==front_move){//判断两次的电机转向是否一样，同时用于判断是否提高电机占空比
           if(rate<1){
          rate+=0.02;
           }
          }else{
          rate=0.3;
          }
         front_move=move;    
    }
void test_transfer(){
    if(state){
    digitalWrite(output13,HIGH);
    }else{
    digitalWrite(output13,LOW);
      }
  }
    
void setup() {
  // put your setup code here, to run once:       
 Serial.begin(9600);
//控制电机方向
 pinMode(output4,OUTPUT);
 pinMode(output5,OUTPUT);

 pinMode(output7,OUTPUT);
 pinMode(output8,OUTPUT);
 //控制LED
 pinMode(output13,OUTPUT);
 //控制舵机
 myservo.attach(output11);
 //设置中断，每10ms进入一次中断服务程序onTimer()
  MsTimer2::set(10,onTimer);
  MsTimer2::start();
}
void loop() {
 //主时钟用来接收上位机
 while(Serial.available()>0){
  inByte=Serial.read();
  temp+=inByte;
  }
  if(temp!=""){
  test_transfer();
  Pi_read=temp.toInt();
  Move=Pi_read>>8;
  Angle=Pi_read-(Move<<8);
  control_rate(Move);
  }
  Serial.println(rate);
   //舵机转向
    myservo.write(Angle);
   delay(200);
   temp="";
  }
