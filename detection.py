import cv2
import serial
import serial.tools.list_ports
import numpy as np
def body_detection():
    point=90
    move = 0
    a=int('0',32)
    b=int('5',32)
    cap = cv2.VideoCapture(-1)
    faceCascade=cv2.CascadeClassifier('/home/xiaolang/Desktop/haarcascade_frontalface_alt.xml')

    if cap.isOpened():
        while True:
             ret,img=cap.read()
             gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
             rect = faceCascade.detectMultiScale(
                     gray,
                     1.3,
                     5
                     )

             img_width= img.shape #获取图像宽度
             if np.all(rect):
                  for x,y,bw,bh in rect:
                      target_middle = x+ bw/2 #获得目标中点的x坐标
                      difference_value = img_width-target_middle #图像和目标的距离
                      if difference_value==a: #如果等于0，那么目标就在图像的中点
                          point=90
                      if difference_value>a: #大于0在左边
                          point=point-1
                      if difference_value<a: #小于0在右边
                          point=point+1
                      front = 0; #用于记录上一次检测框面积大小
                      new_area = bw*bh  #获取当前帧目标面积大小
                      difference_area = new_area - front #两次面积差值，
                      front = new_area
                      if difference_area < b or difference_area > -b:
                         move = 2
                      if difference_area >b :
                         move = 0
                      if difference_area <-b:
                         move = 1
                      return move,point
def serial_write(portx,bps,write_content):
    try:
     ser=serial.Serial(portx,bps,timeout=5)
     result=ser.write(write_content.encode("gbk"))
     ser.close()
    except Exception as e:
     print("---ytichang---",e)
def serial_write(portx,bps,write_content):
    try:
     ser=serial.Serial(portx,bps,timeout=5)

     result=ser.write(write_content.encode("gbk"))

     ser.close()
    except Exception as e:
     print("---ytichang---",e)
def encode(x): #chuan shu bian ma
    n1= x[0] << 8 + x[1]
    return n1
if __name__=='__main__':
    #serial_write("/dev/txd", 9600, encode(body_detection()))
    print(body_detection())